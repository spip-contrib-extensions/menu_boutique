<img align="right" src="https://git.spip.net/spip-contrib-extensions/menu_boutique/raw/branch/master/images/menu_boutique.png">

# Menu Boutique

Un seul menu pour gérer une boutique !

Ce plugin regroupe dans un seul menu les plugins déjà installés pour constituer une boutique.

## Installation & Usage
S'installe sur un SPIP 3.2 minimum, ce plugin utilise le pipeline ajouter_menus.

Une fois le plugin activé le menu Boutique est visible à droite dans le bandeau du haut de l'interface privé de SPIP.
Si vous avez installé certains des plugins d'une boutique, les boutons sont déplacés dans ce menu.
Si un des plugins n'est pas activé, il n'apparait donc pas dans ce menu.

## Ce que ce plugin fait
Il installe les boutons de boutique avec les plugins pris en charge éventuellement affichés dans le menu :
- Produits
- Commandes
- Factures
- Transactions (Bank)
- Stocks
- Modes de livraison
- Coordonnées
- Options de produits
- Coupons
- Billetteries
- Montants

3 boutons en sus par defaut :
- La rubrique choisie dans la config produits ou la rubrique racine avec le nom du site
- Les mots clefs avec les plugins produits et motus pour configurer la rubrique qui recevra les mots-clefs
- Clients (soit vous avez installé Clients ou Contacts soit ce bouton mène à la page visiteurs)

Pour réordonner la liste, allez dans vos préférences (ecrire/?exec=configurer_preferences) et numéroter les entrées.

! Les boutons de configuration des plugins de boutique restent à leur place !


## Ajouter ou retirer un élément du menu
Le bouton de sous-menu SPIP est censé recevoir à minima 3 instructions
- chemin de l'icône
- titre de l'icône
- url de la page

Il faut donc au moins
- que le nom de l'icône soit homonyme au préfixe du plugin, exemple possible monsuperplugin-xx.svg
- que la page cible du lien soit homonyme au préfixe du plugin ?exec=monsuperplugin
- que le fichier de lang paquet-monsuperplugin_fr.php dispose d'une ligne 'monsuperplugin_nom' => 'Mon Super Plugin',

Pas d'inquiétude, si vous avez utilisé le plugin La fabrique, cela doit être le cas.


### 2 méthodes très simples sont possibles pour ajouter un sous-menu

Ajouter une entrée 'monsuperplugin' en commitant dans le plugin menu_boutique directement :
Modification de menu_boutique_pipelines.php

```

function menu_boutique_ajouter_menus( $flux ) {

	//ajouter votre entrée ici pour en faire profiter tout le monde
	$boutons_boutique = array(
		'020. ' => 'produits',
		'030. ' => 'optionsproduits',
		
		(...)
		
		'090. ' => 'billetteries',
		'100. ' => 'coordonnees',
		'110. ' => 'monsuperplugin'
		);

```
La page de lien du bouton doit être homonyme au nom du plugin.
Ne pas oublier de supprimer le bouton déplacé, en vérifiant son nom dans le paquet.xml du plugin

```
unset($flux['menu_edition']->sousmenu['monsuperplugin']);

```

Ajouter une entrée seulement dans votre propre plugin avec le pipeline ajouter_menus qui existe dans SPIP par defaut


```

function prefix_plugin_ajouter_menus($flux){
	
	//s'assurer que menu_boutique est actif
	if(!isset($flux['menu_boutique'])){
		return $flux;
	}
	
	//supprimer une entrée
	unset($flux['menu_boutique']->sousmenu['mon_plugin']);

	//Numérotation pour l'ordre des sous-menus, utiliser par exemple "065. " pour insérer entre deux
	//Ici on créé une entrée à la fin du menu
	$ordre = '110. ';
	$flux['menu_boutique']->sousmenu['mon_plugin'] = 
		new Bouton(
			find_in_theme("images/mon_plugin-xx.svg"), 
			"<span class='d-none'>".$ordre."</span>"._T("paquet-mon_plugin:mon_plugin_nom"),
			generer_url_ecrire( 'mon_plugin' )
		);
							
	return $flux;
}
```

## Changelogs
v1.0.7
- [X] branche master le commit 9dc9fcbdef436c2e55a8c8cac61efda54c61d84f ajoute `use Spip\Admin\Bouton;` pour SPIP4 qu'il faut retirer en SPIP3

v1.0.6 
- [X] le logo du bando principal s'affiche à nouveau en SPIP4.1 après modification du fichier SVG

v1.0.4 et v1.0.5 
- Cafouillage de version

v1.0.3
- [X] ordonner le menu dès le début et non en passant par les prefs auteur

v1.0.2
- [X] Ajout du plugin Coupons
- [X] Ajout du plugin Billetteries
- [X] Ajout du plugin Montants

v1.0.1
- [X] Le bouton d'accès aux mots-cles dépend des plugins produits et motus configurés sur au moins une rubrique
- [X] reprend les logos svg ou png de chaque plugin dans leurs dossiers
- [X] ajout du plugin options paniers
- [X] ajout d'une entrée "clients" par defaut mène à la page Visiteurs, sinon utilise CLients ou bien Contacts

v1.0.0
- [X] Création du plugin menu_boutique
- fix #3eb434cd9a compatible SPIP 3.2


