<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//B
	'boutique' => 'Boutique',
	
	//C
	'clients' => 'Clients',
	
		
	//E
	'tags' => 'Tags',

	// M
	'menu_boutique_titre' => 'Menu Boutique',
);
