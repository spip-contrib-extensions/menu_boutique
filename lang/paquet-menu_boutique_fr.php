<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'menu_boutique_description' => 'Regroupe les menus des plugins pour une boutique SPIP',
	'menu_boutique_nom' => 'Menu Boutique',
	'menu_boutique_slogan' => 'Un seul menu pour gérer une boutique',
);
