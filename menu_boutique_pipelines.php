<?php
/**
 * Utilisations de pipelines par Menu Boutique
 *
 * @plugin     Menu Boutique
 * @copyright  2022
 * @author     Anne-lise Martenot
 * @licence    GNU/GPL
 * @package    SPIP\Menu_boutique\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

use Spip\Admin\Bouton;

/*
 * Pipeline du menu de navigation SPIP privé
 * Déplacer les boutons de certains plugins vers le menu boutique
 */
function menu_boutique_ajouter_menus( $flux ) {
	
	//ajouter votre entrée ici pour en faire profiter tout le monde
	$boutons_boutique = array(
		'020. ' => 'produits',
		'030. ' => 'optionsproduits',
		'040. ' => 'commandes',
		'050. ' => 'factures',
		'060. ' => 'stocks',
		'070. ' => 'coupons',
		'080. ' => 'montants',
		'090. ' => 'billetteries',
		'100. ' => 'coordonnees'
		);	
	
	$titre_rubrique_boutique = textebrut($GLOBALS['meta']['nom_site']);
	$id_rubrique_boutique = '';
	$url_rubrique_boutique = generer_url_ecrire("rubriques");
	$url = '';
	
	//Bouton principal
	$flux['menu_boutique'] = 
	new Bouton(
		icone_menu_boutique( 'menu_boutique' ),  
		_T('menu_boutique:boutique'),
		generer_url_ecrire("navigation","menu=menu_boutique")
	);
	
	//Ajout générique
	foreach( $boutons_boutique as $ordre => $nom_plugin ) {
		if ( test_plugin_actif( $nom_plugin ) 
			) {
					$titre_icone =_T("paquet-$nom_plugin:$nom_plugin"."_nom");
					$url_icone = generer_url_ecrire( $nom_plugin );
					
					//gestion des exceptions
					if ( $nom_plugin == 'stocks' ) {
						$titre_icone = _T( $nom_plugin.':gerer_stocks_titre' );
						$url_icone = generer_url_ecrire( 'gerer_stocks' );
					}
					if ( $nom_plugin == 'factures' ) {
						$titre_icone = _T( $nom_plugin.':titre_page_factures' );
					}
					if ( $nom_plugin == 'optionsproduits' ) {
						$titre_icone = _T( 'options:options_titre' );
						$url_icone = generer_url_ecrire( 'options' );
					}

					//création du bouton
					$flux['menu_boutique']->sousmenu[$nom_plugin] = 
							new Bouton(
								icone_menu_boutique( $nom_plugin ), 
								"<span class='d-none'>".$ordre."</span>".$titre_icone,
								$url_icone
								);
			}
	}
	
	/////  Ajouts particuliers /////
	
	//Rubrique principale des produits
	$ordre = '010. ';
	$flux['menu_boutique']->sousmenu['categories'] = 
	new Bouton(
		icone_menu_boutique( 'rubriques' ),
		"<span class='d-none'>".$ordre."</span>".$titre_rubrique_boutique, 
		$url_rubrique_boutique
	);
	
	//Clients parmi les plugins possibles
	$plugins_clients = array('clients','clientele','contacts');
	foreach($plugins_clients as $nom_plugin){
		switch($nom_plugin){
			case $nom_plugin:
				if ( test_plugin_actif( $nom_plugin) 
					) {
					$titre_icone =_T("paquet-$nom_plugin:$nom_plugin"."_nom");
					$url = generer_url_ecrire( $nom_plugin );
					if ( $nom_plugin == 'clientele'){
						$url = generer_url_ecrire('visiteurs');
					}
					break;
				}
			default: $nom_plugin = 'visiteurs';
			$titre_icone = _T('menu_boutique:clients');
		}
	}
	$ordre = '015. ';
	$flux['menu_boutique']->sousmenu[$nom_plugin] = 
		new Bouton(
			icone_menu_boutique( $nom_plugin ), 
			"<span class='d-none'>".$ordre."</span>".$titre_icone,
			$url
		);

	// Transactions / Bank
	if ( test_plugin_actif( 'bank' ) 
		) {
		$ordre = '045. ';
		$nom_plugin = 'transactions';
		$flux['menu_boutique']->sousmenu[$nom_plugin] = 
		new Bouton(
			icone_menu_boutique( 'transactions' ), 
			"<span class='d-none'>".$ordre."</span>"._T('bank:titre_menu_transactions'),
			generer_url_ecrire( $nom_plugin )
			);
	}
		
	//mots-clefs
	//ssi si un groupe s'applique à la rubrique
	if ( test_plugin_actif( 'motus' ) 
		and autoriser('associermots', 'rubrique', $id_rubrique_boutique) 
		) {
			$ordre = '065. ';
			$nom_plugin = 'mots';
			$flux['menu_boutique']->sousmenu[$nom_plugin] = 
			new Bouton(
				icone_menu_boutique( $nom_plugin ), 
				"<span class='d-none'>".$ordre."</span>"._T('menu_boutique:tags'),
				generer_url_ecrire( $nom_plugin )
			);
		}
 
	// Livraison
	if ( test_plugin_actif( 'livraison' ) 
		) {
		$ordre = '055. ';
		$flux['menu_boutique']->sousmenu['livraisonmodes'] = 
		new Bouton(
			icone_menu_boutique( 'livraison' ), 
			"<span class='d-none'>".$ordre."</span>"._T('livraisonmode:titre_livraisonmodes'),
			generer_url_ecrire("livraisonmodes")
			);
	}
	
	// billets pour billetteries (double entrée)
	if ( test_plugin_actif( 'billetteries' ) 
		) {
		$ordre = '091. ';
		$flux['menu_boutique']->sousmenu['billets'] = 
		new Bouton(
			icone_menu_boutique( 'billetteries' ), 
			"<span class='d-none'>".$ordre."</span>"._T('billet:titre_billets'),
			generer_url_ecrire("billets")
			);
	}
	

	// retirer les menus que l'on a mis dans celui de boutique
	unset($flux['menu_activite']->sousmenu['commandes']);
	unset($flux['menu_activite']->sousmenu['transactions']);
	unset($flux['menu_activite']->sousmenu['factures']);
	unset($flux['menu_activite']->sousmenu['billets']);
	
	unset($flux['menu_edition']->sousmenu['coordonnees']);
	unset($flux['menu_edition']->sousmenu['produits']);
	unset($flux['menu_edition']->sousmenu['livraisonmodes']);
	unset($flux['menu_edition']->sousmenu['gerer_stocks']);
	unset($flux['menu_edition']->sousmenu['coupons']);
	unset($flux['menu_edition']->sousmenu['options']);
	unset($flux['menu_edition']->sousmenu['billetteries']);
	unset($flux['menu_edition']->sousmenu['montants']);
	
	
	return $flux;
	
}

/*
 * @example
 * fouiller_paquet( 'produits' , 'logo' )
*/
function fouiller_paquet( $prefixe , $champ = NULL ) {
	if ( !$prefixe ) {
		return;
	}
	if( !$champ ) {
		$champ = "*";
	}
	$paquet = sql_fetsel($champ, 'spip_paquets','prefixe='.sql_quote($prefixe),'',"`id_paquet` DESC", 1);
	if( isset( $paquet[$champ] ) 
		) {
			return $paquet[$champ];
		}
	
	return $paquet;
}

/*
 *
 * @example
 * icone_menu_boutique( 'produits' )
*/
function icone_menu_boutique( $prefix_plugin ){
	$icone = find_in_theme( "images/$prefix_plugin-xx.svg" );
	//le logo du paquet
	if ( !file_exists( $icone ) 
		) {
		$icone = find_in_path(fouiller_paquet($prefix_plugin, 'logo' ));
		}
	//essai sans le s final
	if ( !file_exists( $icone ) 
		) {
		$prefix_plugin = substr($prefix_plugin, 0, -1);
		$icone = find_in_theme("images/$prefix_plugin-xx.svg");
		}

	return $icone;
}